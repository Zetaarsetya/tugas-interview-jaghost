-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2019 at 09:48 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jagoanhostingdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_05_04_045624_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('Active','Pending','Banned','Loss') COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `gender`, `status`, `email`, `city`, `address`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'Mr. Corbin Stamm', 'Brycen Bednar', 'male', 'Banned', 'judah.dubuque@hotmail.com', 'Thompsonville', '287 Kulas Parkway Suite 820\nEast Eldon, MN 53446', '441.852.2092', '2019-05-03 22:14:41', NULL),
(2, 'Khalil Halvorson', 'Maida Gleichner', 'male', 'Banned', 'crooks.danyka@hotmail.com', 'Connellyhaven', '1754 Natasha Harbor\nPort Carliefurt, VT 03653', '(509) 414-0652 x75572', '2019-05-03 22:14:41', NULL),
(3, 'Dallas Ullrich', 'Duncan Wehner', 'male', 'Active', 'pcummerata@hills.com', 'Port Carleton', '5841 Wilburn Mews Suite 322\nGloverville, OH 76229', '(407) 455-6430 x5186', '2019-05-03 22:14:41', NULL),
(4, 'Kirk Stroman', 'Miss Katrina O''Hara DVM', 'male', 'Loss', 'lavern33@leuschke.com', 'Presleytown', '5280 Maiya Route Suite 428\nDestinifurt, OH 62513-8849', '679-722-8663 x1721', '2019-05-03 22:14:41', NULL),
(5, 'Ms. Amelie DuBuque', 'Scarlett Hintz', 'female', 'Banned', 'earnest.waters@schaefer.com', 'New Danielle', '76140 Creola Isle Apt. 772\nJoaquinfurt, CA 13591', '+1 (950) 720-9448', '2019-05-03 22:14:41', NULL),
(6, 'Leonel Beier DVM', 'Ben Feeney III', 'male', 'Active', 'paxton22@hotmail.com', 'Mullerville', '7555 Aufderhar Walk\nEast Dejahshire, NM 25296', '1-858-571-0721 x08778', '2019-05-03 22:14:41', NULL),
(7, 'Shayna Murray', 'Alden Mertz MD', 'male', 'Loss', 'jeanette27@ernser.com', 'North Kara', '977 Heaven Street\nAdrienchester, IL 48207-2770', '+1 (861) 561-5821', '2019-05-03 22:14:41', NULL),
(8, 'Wilbert Boyer II', 'Zion Bosco', 'male', 'Loss', 'bogan.cordia@gmail.com', 'Willmsstad', '70262 Carlo Lodge Apt. 775\nNorth Valeriemouth, KS 28328-1394', '430.858.3568', '2019-05-03 22:14:41', NULL),
(9, 'Bernadette Aufderhar', 'Prof. Gerardo Kautzer DVM', 'female', 'Banned', 'thermiston@larson.info', 'South Adah', '347 Wendell Coves\nDouglasmouth, TN 26689', '(994) 459-8710', '2019-05-03 22:14:41', NULL),
(10, 'Miss Jaquelin Tromp V', 'Dr. Emmy Buckridge', 'male', 'Loss', 'nicolas.mozell@schaden.com', 'Janismouth', '1999 Boehm Flats Apt. 884\nCaspermouth, LA 63161', '+1.935.369.0738', '2019-05-03 22:14:41', NULL),
(11, 'Jedediah Crist', 'Aurelia Purdy', 'female', 'Active', 'bailey.luigi@hansen.com', 'Bodeville', '4740 Ledner Pass\nNew Jonathon, TX 24259-8508', '+1 (837) 264-5306', '2019-05-03 22:14:41', NULL),
(12, 'Margie McCullough', 'Dr. Rosamond Bayer', 'male', 'Banned', 'claude.schuster@yahoo.com', 'Lake Axelburgh', '44563 Rhoda Garden Suite 725\nPort Ceasarport, NC 02689-6055', '1-556-948-9076', '2019-05-03 22:14:41', NULL),
(13, 'Mrs. Janiya Langworth Jr.', 'Kenton Erdman', 'female', 'Banned', 'block.joy@pfannerstill.info', 'Cieloview', '832 Alyce Valley Apt. 660\nSouth Sabrina, MA 29340', '(993) 231-6077 x960', '2019-05-03 22:14:41', NULL),
(14, 'Dock Dietrich II', 'Asia Runte', 'female', 'Banned', 'rodrick64@hotmail.com', 'East Maybelle', '9527 Koch Manor\nNorth Lavina, WY 27061-3030', '1-738-393-9122 x71330', '2019-05-03 22:14:41', NULL),
(15, 'Dion McCullough', 'Larue Rolfson', 'female', 'Loss', 'cummerata.jeanie@hane.com', 'New Kenny', '887 Velva Causeway\nPort Nadialand, DE 97159-1062', '994-943-6627 x6310', '2019-05-03 22:14:41', NULL),
(16, 'Bani', 'toni', 'female', 'Banned', 'manuel23@frami.biz', 'Wizamouth', '585 Clifton Mountain\nEast Jovanfort, NJ 63069', '+16982377535', '2019-05-03 22:14:41', NULL),
(17, 'Alexandria Keeling', 'Christian Schimmel', 'male', 'Banned', 'bednar.willa@parisian.info', 'South Yasminburgh', '795 Runolfsson Ridges Apt. 917\nPort Janessaberg, ID 80011', '(340) 212-2066', '2019-06-04 22:14:41', NULL),
(18, 'Sherwood Gutmann', 'Ms. Samara Schaefer Jr.', 'female', 'Loss', 'abigail.schiller@yahoo.com', 'Alisonstad', '79710 Bobbie Turnpike Apt. 594\nArmaniland, MD 56942-9215', '+1-487-709-8157', '2019-05-03 22:14:41', NULL),
(19, 'Mrs. Leta Carter', 'Wendell Roberts', NULL, 'Loss', 'braulio61@stamm.net', 'New Carmen', '90454 Thurman Bridge\nLake Shanieton, AZ 07361', '889-413-8744 x708', '2019-05-03 22:14:41', NULL),
(20, 'Kennedi Hodkiewicz', 'Lauren Pollich', 'female', 'Banned', 'abigale.vandervort@yahoo.com', 'Nicklausbury', '853 Ryann Island Apt. 224\nEast Rocky, KY 19323', '+1.281.895.3175', '2019-05-03 22:14:41', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
