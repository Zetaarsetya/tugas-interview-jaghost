<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\users as User;
//elogquent = metode crud menggunakan sebuah model agar lebih rapi
class ApiController extends Controller
{
    public function index()
    {
        $users = User::All();
        $limit = 10;
        $select = User::paginate($limit);
        $collection = $select->getCollection();
        $total_data = count($users);   
        $total_page = $select->lastPage();
        //total halaman
        $current_page = $select->currentPage();
        //pada halaman berapa
        $whereColumn = User::where([
            ['firstname', '=', 'Bani'],
            ['lastname', '=', 'toni']
        ])->get();
        $whereNull = User::whereNull('gender')->get();
        $whereNotIn= User::whereNotIn('status',['Banned','Disabled'])->get();
        $whereIn = User::whereIn('status',['Active','Banned'])->get();
        $whereNotBetween = User::whereNotBetween('id',[11,20])->get();
        $whereBetween = User::whereBetween('id',[11,20])->get();;
        $orWhere = User::where('id', '!=', 0)->get();
        $function = [$whereNotIn,$whereIn];
        $orderName = User::orderBy('firstname', 'DESC')->get();
        $orderDate = User::orderBy('created_at', 'DESC')->get();
        $order = [
            ['field' => 'name','order' => $orderName],
            ['field' => 'date','order' => $orderDate]
        ];
        $conditons = [
            ['type' => 'whereColumn','data' => $whereColumn],
            ['type' => 'whereNull','data' => $whereNull],
            ['type' => 'whereNotIn','data' => $whereNotIn],
            ['type' => 'whereIn','data' => $whereIn],
            ['type' => 'whereNotBetween','data' => $whereNotBetween],
            ['type' => 'whereBetween','data' => $whereBetween],
            ['type' => 'orWhere','data' => $orWhere],
            ['type' => 'orWhere','data' => $function],
        ];
        if($total_data > 0){
            $res['select'] = $collection;
            $res['limit'] = $limit;
            $res['conditions'] = $conditons;
            $res['current_page'] = $current_page;
            $res['order'] = $order;
            return response($res);
        }
        else{
            $res['message'] = "Empty!";
            return response($res);
        }
    }
}
