<?php

use Illuminate\Database\Seeder;
// mengisi / menginsert data ke database / table
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker\Factory::create();
        $timenow = Carbon\Carbon::now();
        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            	DB::table('users')->insert([ 
                'firstname' => $faker->name,
                //faker = library untuk membuat data dummy(palsu)
                'lastname' => $faker->name,
                'gender' => $faker->randomElement(['male' ,'female']),
                'status' => $faker->randomElement(['Active','Pending','Banned','Loss']),
                'email' => $faker->unique()->email,
                'city' => $faker->city,
                'phone' => $faker->phoneNumber,
                'address' => $faker->address,
                'created_at' => $timenow->toDateTimeString()
            ]);
        }
    }
}
