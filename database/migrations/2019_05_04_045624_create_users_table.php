<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

//migrasi = memudahkan untuk berpindah database dan membuat struktur table
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstname');
            $table->string('lastname');
            $table->enum('gender',['male','female']);
            $table->enum('status',['Active','Pending','Banned','Loss']);
            $table->string('email');
            $table->string('city');
            $table->string('address');
            $table->string('phone');
            $table->timestamps();
            //otomatis created_at => date dan updated_at => date

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
